﻿using CoffeeChain.Domain.Entities;
using Core.Messages;
using System.Collections.Generic;
using Core.Core.DomainObjects;
using FluentValidation;
using System;

namespace CoffeeChain.Application.Commands
{
    public class AtualizarSaidasCommand : CommandReturnId
    {

        public AtualizarSaidasCommand()
        {

        }

        public AtualizarSaidasCommand(int destinoProdutorId, int destinoPropriedadeId, int destinoEmpresaId, int entradaId, int destinoArmazemId, DateTime dataSaida, decimal qtdSacas, int tipoSaida, decimal precoSaida, decimal custoSaida, string nfeSaida, string embalagemSaida)
        {
            DestinoProdutorId = destinoProdutorId;
            DestinoPropriedadeId = destinoPropriedadeId;
            DestinoEmpresaId = destinoEmpresaId;
            EntradaId = entradaId;
            DestinoArmazemId = destinoArmazemId;
            DataSaida = dataSaida;
            QtdSacas = qtdSacas;
            TipoSaida = tipoSaida;
            PrecoSaida = precoSaida;
            CustoSaida = custoSaida;
            NfeSaida = nfeSaida;
            EmbalagemSaida = embalagemSaida;
        }

        public int DestinoProdutorId { get; set; }
        public int DestinoPropriedadeId { get; set; }
        public int DestinoEmpresaId { get; set; }
        public int EntradaId { get; set; }
        public int DestinoArmazemId { get; set; }
        public DateTime DataSaida { get; set; }
        public decimal QtdSacas { get; set; }
        public int TipoSaida { get; set; }
        public decimal PrecoSaida { get; set; }
        public decimal CustoSaida { get; set; }
        public string NfeSaida { get; set; }
        public string EmbalagemSaida { get; set; }





        public override bool EhValido()
        {
            var result = new EmpresaValidation().Validate(this);
            this.ValidationResult = new Core.Core.DomainObjects.ValidationResult(result.Errors);
            return ValidationResult.IsValid;
        }

        public class EmpresaValidation : AbstractValidator<AtualizarSaidasCommand>
        {

            public EmpresaValidation()
            {
                RuleFor(c => c.DestinoProdutorId)
                   .NotNull().WithMessage("o campo {PropertyName} deve ser informado")
                   .NotEmpty().WithMessage("o campo {PropertyName} deve ser informado");

                RuleFor(c => c.DestinoPropriedadeId)
                 .NotNull().WithMessage("o campo {PropertyName} deve ser informado")
                 .NotEmpty().WithMessage("o campo {PropertyName} deve ser informado");


                RuleFor(c => c.DestinoEmpresaId)
                 .NotNull().WithMessage("o campo {PropertyName} deve ser informado")
                 .NotEmpty().WithMessage("o campo {PropertyName} deve ser informado");


                RuleFor(c => c.EntradaId)
                 .NotNull().WithMessage("o campo {PropertyName} deve ser informado")
                 .NotEmpty().WithMessage("o campo {PropertyName} deve ser informado");


                RuleFor(c => c.DestinoArmazemId)
                 .NotNull().WithMessage("o campo {PropertyName} deve ser informado")
                 .NotEmpty().WithMessage("o campo {PropertyName} deve ser informado");


                RuleFor(c => c.DataSaida)
                 .NotNull().WithMessage("o campo {PropertyName} deve ser informado")
                 .NotEmpty().WithMessage("o campo {PropertyName} deve ser informado");


                RuleFor(c => c.QtdSacas)
                .NotNull().WithMessage("o campo {PropertyName} deve ser informado")
                .NotEmpty().WithMessage("o campo {PropertyName} deve ser informado");


                RuleFor(c => c.TipoSaida)
                .NotNull().WithMessage("o campo {PropertyName} deve ser informado")
                .NotEmpty().WithMessage("o campo {PropertyName} deve ser informado");


                RuleFor(c => c.PrecoSaida)
               .NotNull().WithMessage("o campo {PropertyName} deve ser informado")
               .NotEmpty().WithMessage("o campo {PropertyName} deve ser informado");


                RuleFor(c => c.CustoSaida)
               .NotNull().WithMessage("o campo {PropertyName} deve ser informado")
               .NotEmpty().WithMessage("o campo {PropertyName} deve ser informado");


                RuleFor(c => c.NfeSaida)
              .NotNull().WithMessage("o campo {PropertyName} deve ser informado")
              .NotEmpty().WithMessage("o campo {PropertyName} deve ser informado")
              .MaximumLength(20).WithMessage("o campo {PropertyName} deve possuir no máximo {MaxLength} caracteres");

                RuleFor(c => c.EmbalagemSaida)
              .NotNull().WithMessage("o campo {PropertyName} deve ser informado")
              .NotEmpty().WithMessage("o campo {PropertyName} deve ser informado")
              .MaximumLength(20).WithMessage("o campo {PropertyName} deve possuir no máximo {MaxLength} caracteres");

            }
        }
    }
}
