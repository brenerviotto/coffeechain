﻿using CoffeeChain.Domain.Entities;
using Core.Messages;
using System.Collections.Generic;
using Core.Core.DomainObjects;
using FluentValidation;

namespace CoffeeChain.Application.Commands
{
    public class AdicionarArmazemCommand : CommandReturnId
    {

        public AdicionarArmazemCommand()
        {

        }

        public AdicionarArmazemCommand(int armazemId, int empresaId, string telefoneArmazem, string enderecoArmazem, string cidadeArmazem, string ufArmazem, string emailArmazem)
        {
            ArmazemId = armazemId;
            EmpresaId = empresaId;
            TelefoneArmazem = telefoneArmazem;
            EnderecoArmazem = enderecoArmazem;
            CidadeArmazem = cidadeArmazem;
            UfArmazem = ufArmazem;
            EmailArmazem = emailArmazem;
        }

        public int ArmazemId { get; set; }
        public int EmpresaId { get; set; }
        public string TelefoneArmazem { get; set; }
        public string EnderecoArmazem { get; set; }
        public string CidadeArmazem { get; set; }
        public string UfArmazem { get; set; }
        public string EmailArmazem { get; set; }
        




        public override bool EhValido()
        {
            var result = new EmpresaValidation().Validate(this);
            this.ValidationResult = new Core.Core.DomainObjects.ValidationResult(result.Errors);
            return ValidationResult.IsValid;
        }

        public class EmpresaValidation : AbstractValidator<AdicionarArmazemCommand>
        {

            public EmpresaValidation()
            {
                RuleFor(c => c.EmpresaId)
                   .NotNull().WithMessage("o campo {PropertyName} deve ser informado")
                   .NotEmpty().WithMessage("o campo {PropertyName} deve ser informado");

                RuleFor(c => c.TelefoneArmazem)
                   .NotNull().WithMessage("o campo {PropertyName} deve ser informado")
                   .NotEmpty().WithMessage("o campo {PropertyName} deve ser informado")
                   .MaximumLength(20).WithMessage("o campo {PropertyName} deve possuir no máximo {MaxLength} caracteres");

                RuleFor(c => c.EnderecoArmazem)
                  .NotNull().WithMessage("o campo {PropertyName} deve ser informado")
                  .NotEmpty().WithMessage("o campo {PropertyName} deve ser informado")
                  .MaximumLength(200).WithMessage("o campo {PropertyName} deve possuir no máximo {MaxLength} caracteres");

                RuleFor(c => c.CidadeArmazem)
                  .NotNull().WithMessage("o campo {PropertyName} deve ser informado")
                  .NotEmpty().WithMessage("o campo {PropertyName} deve ser informado")
                  .MaximumLength(100).WithMessage("o campo {PropertyName} deve possuir no máximo {MaxLength} caracteres");

                RuleFor(c => c.UfArmazem)
                  .NotNull().WithMessage("o campo {PropertyName} deve ser informado")
                  .NotEmpty().WithMessage("o campo {PropertyName} deve ser informado")
                  .MaximumLength(2).WithMessage("o campo {PropertyName} deve possuir no máximo {MaxLength} caracteres");

                RuleFor(c => c.EmailArmazem)
                  .NotNull().WithMessage("o campo {PropertyName} deve ser informado")
                  .NotEmpty().WithMessage("o campo {PropertyName} deve ser informado")
                  .MaximumLength(100).WithMessage("o campo {PropertyName} deve possuir no máximo {MaxLength} caracteres");


            }
        }
    }
}
