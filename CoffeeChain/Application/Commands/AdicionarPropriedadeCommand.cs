﻿using CoffeeChain.Domain.Entities;
using Core.Messages;
using System.Collections.Generic;
using Core.Core.DomainObjects;
using FluentValidation;

namespace CoffeeChain.Application.Commands
{
    public class AtualizarPropriedadeCommand : CommandReturnId
    {

        public AtualizarPropriedadeCommand()
        {

        }

        public AtualizarPropriedadeCommand(int produtorId, string nomeFazenda, string cNPJFazenda, string inscEstadual, string telefonePropriedade, string enderecoPropriedade, string cidadePropriedade, string ufPropriedade, string emailPropriedade, string cepPropriedade)
        {
            ProdutorId = produtorId;
            NomeFazenda = nomeFazenda;
            CNPJFazenda = cNPJFazenda;
            InscEstadual = inscEstadual;
            TelefonePropriedade = telefonePropriedade;
            EnderecoPropriedade = enderecoPropriedade;
            CidadePropriedade = cidadePropriedade;
            UfPropriedade = ufPropriedade;
            EmailPropriedade = emailPropriedade;
            CepPropriedade = cepPropriedade;
        }

        public int ProdutorId { get; set; }
        public string NomeFazenda { get; set; }
        public string CNPJFazenda { get; set; }
        public string InscEstadual { get; set; }
        public string TelefonePropriedade { get; set; }
        public string EnderecoPropriedade { get; set; }
        public string CidadePropriedade { get; set; }
        public string UfPropriedade { get; set; }
        public string EmailPropriedade { get; set; }
        public string CepPropriedade { get; set; }


        public override bool EhValido()
        {
            var result = new PropriedadeValidation().Validate(this);
            this.ValidationResult = new Core.Core.DomainObjects.ValidationResult(result.Errors);
            return ValidationResult.IsValid;
        }

        public class PropriedadeValidation : AbstractValidator<AtualizarPropriedadeCommand>
        {

            public PropriedadeValidation()
            {
                RuleFor(c => c.ProdutorId)
                   .NotNull().WithMessage("o campo {PropertyName} deve ser informado")
                   .NotEmpty().WithMessage("o campo {PropertyName} deve ser informado");

                RuleFor(c => c.NomeFazenda)
                 .NotNull().WithMessage("o campo {PropertyName} deve ser informado")
                 .NotEmpty().WithMessage("o campo {PropertyName} deve ser informado")
                 .MaximumLength(100).WithMessage("o campo {PropertyName} deve possuir no máximo {MaxLength} caracteres");

                RuleFor(c => c.CNPJFazenda)
                 .NotNull().WithMessage("o campo {PropertyName} deve ser informado")
                 .NotEmpty().WithMessage("o campo {PropertyName} deve ser informado")
                .MaximumLength(20).WithMessage("o campo {PropertyName} deve possuir no máximo {MaxLength} caracteres");

                RuleFor(c => c.InscEstadual)
                 .NotNull().WithMessage("o campo {PropertyName} deve ser informado")
                 .NotEmpty().WithMessage("o campo {PropertyName} deve ser informado")
                .MaximumLength(20).WithMessage("o campo {PropertyName} deve possuir no máximo {MaxLength} caracteres");

                RuleFor(c => c.TelefonePropriedade)
                 .NotNull().WithMessage("o campo {PropertyName} deve ser informado")
                 .NotEmpty().WithMessage("o campo {PropertyName} deve ser informado")
                 .MaximumLength(20).WithMessage("o campo {PropertyName} deve possuir no máximo {MaxLength} caracteres");

                RuleFor(c => c.EnderecoPropriedade)
                 .NotNull().WithMessage("o campo {PropertyName} deve ser informado")
                 .NotEmpty().WithMessage("o campo {PropertyName} deve ser informado")
                 .MaximumLength(200).WithMessage("o campo {PropertyName} deve possuir no máximo {MaxLength} caracteres");

                RuleFor(c => c.CidadePropriedade)
                .NotNull().WithMessage("o campo {PropertyName} deve ser informado")
                .NotEmpty().WithMessage("o campo {PropertyName} deve ser informado")
                .MaximumLength(50).WithMessage("o campo {PropertyName} deve possuir no máximo {MaxLength} caracteres");

                RuleFor(c => c.UfPropriedade)
                .NotNull().WithMessage("o campo {PropertyName} deve ser informado")
                .NotEmpty().WithMessage("o campo {PropertyName} deve ser informado")
                .MaximumLength(2).WithMessage("o campo {PropertyName} deve possuir no máximo {MaxLength} caracteres");

                RuleFor(c => c.EmailPropriedade)
               .NotNull().WithMessage("o campo {PropertyName} deve ser informado")
               .NotEmpty().WithMessage("o campo {PropertyName} deve ser informado")
               .MaximumLength(100).WithMessage("o campo {PropertyName} deve possuir no máximo {MaxLength} caracteres");

                RuleFor(c => c.CepPropriedade)
               .NotNull().WithMessage("o campo {PropertyName} deve ser informado")
               .NotEmpty().WithMessage("o campo {PropertyName} deve ser informado")
               .MaximumLength(20).WithMessage("o campo {PropertyName} deve possuir no máximo {MaxLength} caracteres");

            }
        }
    }
}
